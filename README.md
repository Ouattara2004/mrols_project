
# Mrols


The goal of Mrols is to estimate a linear moddal using map reduce OLS

## Installation

You can install the released version of Mrols from : https://gitlab.com/Ouattara2004/mrols_project.git 


``` r
install.packages("Mrols")
```

## Example

This is a basic example which shows you how to solve a common problem:

set.seed(001)
X = data.frame(x1 = rnorm(100),
               x2 = rnorm(100))
y = as.vector(rnorm(100))

res = Main2(X, y, 10)
res

